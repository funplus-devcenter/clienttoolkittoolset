import sys
import subprocess
import platform
import pathlib
import os
import shutil

CONFIG_REPO_PATH = None
PROTOKITGO_CONFIG_NAME = None
RES_PLATFORM = None
RES_TYPES = None  # 需要上传的资源类型后缀，比如'.x,.zip'
UPLOAD_FOLDER = None  # 需要上传的文件夹
REMOTE_DIR = None  # 上传的远端根目录
APP_VERSION = None  # App 版本，例如：1.9.3
RES_VERSION = None  # 资源显示的版本，例如：17899
NO_UPLOAD = None  # 是否上传云端

PROTOKITGO_CONFIG_PATH = None


def hardreseet_config_repo():
    try:
        git_work_dir = pathlib.Path(CONFIG_REPO_PATH)
        print(git_work_dir)
        cmd = ['git', 'fetch']
        subprocess.run(cmd, cwd=git_work_dir)
        cmd = ['git', 'reset', '--hard', 'FETCH_HEAD']
        subprocess.run(cmd, cwd=git_work_dir)
    except Exception as ex:
        print('Reset config repo failure!')
        raise ex
    return


# def copy_or_replace_protokityaml_to_upload_folder():
#     cur_dir = pathlib.Path(os.getcwd())
#     src_cfg = cur_dir.as_posix() + '/protokitgo.yaml'
#     work_dir = pathlib.Path(UPLOAD_FOLDER)
#     target_cfg_path = work_dir.as_posix() + '/protokitgo.yaml'
#     if os.path.exists(target_cfg_path):
#         os.remove(target_cfg_path)
#
#     print('Start copy protokitgo.yaml !')
#     try:
#         shutil.copyfile(src_cfg, target_cfg_path)
#     except Exception as ex:
#         print(ex)
#         raise ex


def copy_version_file_to_native():
    print('VERSION_FILE_PATH : %s .' % get_version_file_config_path())
    shared_version_file_path = pathlib.Path(get_version_file_config_path())
    print(shared_version_file_path.parent.as_posix())
    work_dir = pathlib.Path(UPLOAD_FOLDER)

    native_version_file_folder_path_str = work_dir.as_posix() + '/gen/rawdata/server'
    native_version_file_folder_path = pathlib.Path(native_version_file_folder_path_str)

    if not os.path.exists(native_version_file_folder_path):
        print('Create version folder , path : %s .' % native_version_file_folder_path.as_posix())
        os.makedirs(native_version_file_folder_path.as_posix())

    native_version_file_path_str = native_version_file_folder_path.as_posix() + '/resource_versions.release'
    native_version_file_path = pathlib.Path(native_version_file_path_str)

    print('shared_version_file_path : %s .' % shared_version_file_path.as_posix())
    print('native_version_file_path : %s .' % native_version_file_path.as_posix())

    if os.path.exists(native_version_file_path):
        os.remove(native_version_file_path.as_posix())

    try:
        shutil.copyfile(shared_version_file_path.as_posix(), native_version_file_path.as_posix())
        print('Copy resource_versions.release successful!')
    except Exception as ex:
        print(type(ex))
        print(ex.args)
        print(ex)
        raise ex


def call_protokitgo():
    curDir = pathlib.Path(os.getcwd())
    print(curDir)
    protokitgo_path = None
    if platform.system() == 'Windows':
        protokitgo_path = '%s/protokitgo/Windows/protokitgo.exe' % curDir.as_posix()
    else:
        protokitgo_path = '%s/protokitgo/MacOS/protokitgo' % curDir.as_posix()
        getPermissionCmd = ['sudo', 'chmod', 'a+x', protokitgo_path]
        print(getPermissionCmd)
        try:
            subprocess.run(getPermissionCmd)
        except Exception as ex:
            print(ex)
            print('Get execute right failure')
            raise ex

    if REMOTE_DIR == '**NOROOT**':
        cmd = [protokitgo_path,
               'release',
               'upload_res',
               '--config=%s' % PROTOKITGO_CONFIG_PATH,
               '--res_type=%s' % RES_PLATFORM,
               '--upload_types=%s' % RES_TYPES,
               '--upload_dir=%s' % UPLOAD_FOLDER,
               '--app_version=%s' % APP_VERSION,
               '--res_version=%s' % RES_VERSION,
               '--no_upload=%s' % NO_UPLOAD
               ]
    else:
        cmd = [protokitgo_path,
               'release',
               'upload_res',
               '--config=%s' % PROTOKITGO_CONFIG_PATH,
               '--res_type=%s' % RES_PLATFORM,
               '--upload_types=%s' % RES_TYPES,
               '--upload_dir=%s' % UPLOAD_FOLDER,
               '--remote_dir=%s' % REMOTE_DIR,
               '--app_version=%s' % APP_VERSION,
               '--res_version=%s' % RES_VERSION,
               '--no_upload=%s' % NO_UPLOAD
               ]
    print(cmd)
    try:
        subprocess.run(cmd, cwd=UPLOAD_FOLDER)
    except Exception as ex:
        print(ex)
        raise ex


def get_version_file_config_path():
    config_path = CONFIG_REPO_PATH + '/resource_versions.release'
    return config_path


def get_version_file_config_gen_path():
    config_path = CONFIG_REPO_PATH + '/gen/rawdata/server/resource_versions.release'
    return config_path


# def copy_version_file_to_conf_and_commit():
#     if NO_UPLOAD == 'true':
#         print('no upload , skip copy version file to config path and commit !')
#         return
#
#     target_version_file_path = pathlib.Path(get_version_file_config_path())
#     work_dir = pathlib.Path(UPLOAD_FOLDER)
#     native_version_file_path_str = work_dir.as_posix() + '/gen/rawdata/server/resource_versions.release'
#     native_version_file_path = pathlib.Path(native_version_file_path_str)
#
#     print('target_version_file_path : %s .' % target_version_file_path.as_posix())
#     print('native_version_file_path : %s .' % native_version_file_path.as_posix())
#     print('Start copy version file to target git path !')
#     try:
#         if os.path.exists(target_version_file_path):
#             os.remove(target_version_file_path.as_posix())
#         shutil.copyfile(native_version_file_path.as_posix(), target_version_file_path.as_posix())
#         print('Copy resource_versions.release to conf folder successful!')
#     except Exception as ex:
#         print(type(ex))
#         print(ex.args)
#         print(ex)
#         raise ex
#
#     target_version_file_path = pathlib.Path(get_version_file_config_gen_path())
#     print('target_version_file_gen_ath : %s .' % target_version_file_path.as_posix())
#     print('native_version_file_path : %s .' % native_version_file_path.as_posix())
#     print('Start copy version file to target git gen path !')
#     try:
#         if os.path.exists(target_version_file_path):
#             os.remove(target_version_file_path.as_posix())
#         shutil.copyfile(native_version_file_path.as_posix(), target_version_file_path.as_posix())
#         print('Copy resource_versions.release to conf gen folder successful!')
#     except Exception as ex:
#         print(type(ex))
#         print(ex.args)
#         print(ex)
#         raise ex
#
#     try:
#         #git_work_dir = target_version_file_path.parent.as_posix()
#         git_work_dir = pathlib.Path(CONFIG_REPO_PATH).as_posix()
#         print(git_work_dir)
#         cmd = ['git', 'add', '.']
#         subprocess.run(cmd, cwd=git_work_dir)
#         cmd = ['git', 'commit', '-m',
#                'Upload version ! Appversion : %s  ResVersion : %s . ' % (APP_VERSION, RES_VERSION)]
#         subprocess.run(cmd, cwd=git_work_dir)
#         cmd = ['git', 'push', '-v']
#         subprocess.run(cmd, cwd=git_work_dir)
#     except Exception as ex:
#         print('git upload failure!')
#         raise ex
#     return

def commit_conf_modifies():
    if NO_UPLOAD == 'true':
        print('no upload , skip copy version file to config path and commit !')
        return
    try:
        git_work_dir = pathlib.Path(CONFIG_REPO_PATH).as_posix()
        print(git_work_dir)
        cmd = ['git', 'add', '.']
        subprocess.run(cmd, cwd=git_work_dir)
        cmd = ['git', 'commit', '-m',
               'Upload version ! Appversion : %s  ResVersion : %s . ' % (APP_VERSION, RES_VERSION)]
        subprocess.run(cmd, cwd=git_work_dir)
        cmd = ['git', 'push', '-v']
        subprocess.run(cmd, cwd=git_work_dir)
    except Exception as ex:
        print('git upload failure!')
        raise ex
    return


COMMAND_LINE_ARGS_COUNT = 10


def _cli_error():
    print('The command line args count is not %d !' % COMMAND_LINE_ARGS_COUNT)
    sys.exit(1)


def parser_commandline_args(argv):
    print(argv)
    if len(argv) != COMMAND_LINE_ARGS_COUNT:
        _cli_error()

    global CONFIG_REPO_PATH
    global PROTOKITGO_CONFIG_NAME
    global RES_PLATFORM
    global RES_TYPES
    global UPLOAD_FOLDER
    global REMOTE_DIR
    global APP_VERSION
    global RES_VERSION
    global NO_UPLOAD
    global PROTOKITGO_CONFIG_PATH

    CONFIG_REPO_PATH = argv[1]
    PROTOKITGO_CONFIG_NAME = argv[2]
    RES_PLATFORM = argv[3]
    RES_TYPES = argv[4]
    UPLOAD_FOLDER = argv[5]
    REMOTE_DIR = argv[6]
    APP_VERSION = argv[7]
    RES_VERSION = argv[8]
    NO_UPLOAD = argv[9].strip()
    print('NoUpload : %s ' % NO_UPLOAD)

    configRepoPath = pathlib.Path(CONFIG_REPO_PATH)
    PROTOKITGO_CONFIG_PATH = '%s/%s' % (configRepoPath.as_posix(), PROTOKITGO_CONFIG_NAME)
    print('Proto config path : %s' % PROTOKITGO_CONFIG_PATH)


def excute(argv):
    parser_commandline_args(argv)
    hardreseet_config_repo()
    # copy_or_replace_protokityaml_to_upload_folder()
    # copy_version_file_to_native()
    call_protokitgo()
    # copy_version_file_to_conf_and_commit()
    commit_conf_modifies()


if __name__ == '__main__':
    excute(sys.argv)
