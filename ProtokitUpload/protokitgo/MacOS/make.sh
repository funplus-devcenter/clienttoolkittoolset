#!/bin/bash

SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do SOURCE="$(readlink "$SOURCE")"; done
DIR_CURR="$(cd -P "$(dirname "$SOURCE")/" && pwd)"
release_version=$(cat "${DIR_CURR}"/version)

run_test() {
    go run cmd/main.go export
    go run cmd/main.go pb2db
    sh example/tests/test_all.sh
}

build() {
  go run cmd/main.go export
  go run cmd/main.go pb2db
  sh example/tests/test_all.sh
  if [  $? -ne 0 ];then
    echo "test error"
    exit 1
  fi
  curr=`pwd`
  rm -rf dist
  BUILD_DATE=${BUILD_DATE:-$(date +%Y%m%d-%H:%M:%S)}
  revision=$(git rev-parse --short HEAD 2>/dev/null || echo 'unknown')
  branch=$(git rev-parse --abbrev-ref HEAD 2>/dev/null || echo 'unknown')
  ldflags="-s -w -X bitbucket.org/funplus/golib/version.Version=$1"
  ldflags="$ldflags -X bitbucket.org/funplus/golib/version.Revision=$revision"
  ldflags="$ldflags -X bitbucket.org/funplus/golib/version.Branch=$branch"
  ldflags="$ldflags -X bitbucket.org/funplus/golib/version.BuildDate=$BUILD_DATE"

  suffixs=('' '' '.exe')
  arr=(linux osx win)
  gooss=(linux darwin windows)
  for i in ${!arr[@]};
  do
    os=${arr[$i]}
    goos=${gooss[$i]}
    mkdir -p dist/${os}
    mkdir -p dist/${os}/bin/${os}
    mkdir -p dist/${os}/example
    cp -r bin/${os}/* dist/${os}/bin/${os}/
    cp -r sdk dist/${os}/
    cp -r example/excel example/protos example/tests example/protokitgo.yaml dist/${os}/example/
    cp Makefile version dist/${os}/
    CGO_ENABLED=0 GOOS=${goos} GOARCH=amd64 go build  -ldflags "${ldflags}" -o dist/${os}/protokitgo${suffixs[$i]} ./cmd/main.go
    upx dist/${os}/protokitgo${suffixs[$i]}
  done
}

install() {
  sudo rm -rf /opt/protokitgo
  sudo rm -rf /usr/local/bin/protokitgo
  sudo mv `pwd` /opt/protokitgo
  if [ "`uname`" = "Darwin" ]
  then
    cd /usr/local/bin && ln -s /opt/protokitgo/protokitgo
  else
    exists=`grep "protokitgo" ~/.bash_profile | wc -l`
    if [ $exists -gt 0 ]
    then
      exit 0
    fi
    echo "PATH=\$PATH:/opt/protokit:/opt/protokitgo
export PATH" >> ~/.bash_profile
  fi
}

release() {
  arr=(linux osx win)
  curr=`pwd`
  for os in ${arr[@]};
  do
      cd $curr/dist/${os}/ && zip -r protokitgo_${os}_$1.zip * && mv protokitgo_${os}_$1.zip ../
  done
  echo "zip success!"
  cd $curr
  echo "#!/bin/bash
os=linux
if [ \"\`uname\`\" = \"Darwin\" ]
then
  os=osx
else
  yum install -y ntpdate unzip
  ntpdate 0.pool.ntp.org
fi
rm protokitgo_\${os}_$1.zip
wget https://zhongtai.s3.amazonaws.com/software/protokitgo/protokitgo_\${os}_$1.zip
curr_dir=\`pwd\`
unzip -d protokitgo_\${os} protokitgo_\${os}_$1.zip
cd protokitgo_\${os}
make install
cd \$curr_dir
rm -rf protokitgo_\${os}_$1.zip
rm -rf protokitgo_\${os}
rm -rf install.sh
  " > dist/install.sh
  for os in ${arr[@]};
  do
      echo "upload protokitgo_${os}_$1.zip"
  done
  for os in ${arr[@]};
  do
      echo "Download URL: https://zhongtai.s3.amazonaws.com/software/protokitgo/protokitgo_${os}_$1.zip"
  done
  echo "Download URL: https://zhongtai.s3.amazonaws.com/software/protokitgo/install.sh"
}

case "$1" in
  test)
    run_test
  ;;
  build)
    build $release_version
  ;;
  release)
    release $release_version
  ;;
  install)
    install
  ;;
esac
