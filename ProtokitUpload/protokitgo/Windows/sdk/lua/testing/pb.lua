local json = require "json"

function encode(name,tbl)
    return json.encode({name=name,msg=tbl})
end

function decode(name,bytes)
    local got = json.decode(bytes)
    return got.msg
end

local schemaMap = {}
function load(schema,name)
    schemaMap[name] = schema
end

return {
    encode = encode,
    decode = decode,
    load = load,
}