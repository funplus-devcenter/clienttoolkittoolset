package.path ="../../../sample/gen/lua/?.lua;../?.lua;".. package.path

local testing = require 'testing'
local metadata = require "metadata"


testing.metadata = function ()
    local md = metadata.new()
     testing.assert(md:len()==0)
     md:set("k1","v1")
     testing.assert(md:len()==1)
     testing.equal(md:getFirst("k1"),"v1")
     md:set("k1","v1","v2")
     testing.assert(md:len()==1)
     testing.equal(md:get("k1"),{"v1","v2"})
     testing.equal(md:getFirst("k1"),"v1")
     md:append("k1","v3","v4")
     testing.equal(md:get("k1"),{"v1","v2","v3","v4"})
     md:append("k2","s1","s2")
     testing.equal(md:get("k2"),{"s1","s2"})
     md:del("k2")
     testing.equal(md:get("k2"),nil)
end

testing.append = function()
    for _,test in ipairs({
        {
            metadata.fromPairs("My-Optional-Header", "42"),
            "Other-Key",
            {"1"},
            metadata.fromPairs("my-optional-header", "42", "other-key", "1")
        },
        {
            metadata.fromPairs("My-Optional-Header", "42"),
            "my-OptIoNal-HeAder",
            {"1", "2", "3"},
            metadata.fromPairs("my-optional-header", "42", "my-optional-header", "1",
                                   				"my-optional-header", "2", "my-optional-header", "3")
        },
        {
            metadata.fromPairs("My-Optional-Header", "42"),
            "my-OptIoNal-HeAder",
            {},
            metadata.fromPairs("my-optional-header", "42"),
        },
    }) do
        test[1]:append(test[2],test[3])
        testing.equal(test[1],test[4])
    end
end

testing.get = function ()
    for _,test in ipairs({
        {metadata.fromPairs("My-Optional-Header", "42"),"My-Optional-Header",{"42"},"42"},
		{metadata.fromPairs("Header", "42", "Header", "43", "Header", "44", "other", "1"), "HEADER", {"42", "43", "44"},"42"},
		{metadata.fromPairs("HEADER", "10"),  "HEADER",{"10"},"10"},
    }) do
        testing.equal(test[1]:get(test[2]),test[3])
        testing.equal(test[1]:getFirst(test[2]),test[4])
    end
end

testing.set = function()
    for _,test in ipairs({
        { {"k1", "v1", "k1", "v2"},"x_create_time",1111, metadata.new({["k1"]={"v1","v2"},["x_create_time"]={"1111"}}) },
    }) do
        local got1 = metadata.fromPairs(table.unpack(test[1]))
        got1:set(test[2],test[3])
        testing.equal(got1,test[4])
        local got2 = metadata.fromPairs(test[1])
        got2:set(test[2],test[3])
        testing.equal(got2,test[4])
    end
end

testing.fromPairs = function()
    for _,test in ipairs({
        {{},metadata.new()},
        { {"k1", "v1", "k1", "v2"},metadata.new({["k1"]={"v1","v2"}}) },
    }) do
        testing.equal(metadata.fromPairs(table.unpack(test[1])),test[2])
    end
end

testing.toPairs = function()
    for _,test in ipairs({
    {metadata.new(),{}},
        {metadata.new({["k1"]={"v1","v2"}}), {"k1", "v1", "k1", "v2"},},
    }) do
        testing.equal(metadata.toPairs(test[1]),test[2])
    end
end

testing.copy = function()
    local md1 = metadata.fromPairs("k1","v1")
    local md2 = md1:copy()
    testing.equal(md1,md2)
    md2:set("k1","v2")
    testing.not_equal(md1,md2)

end

testing.join = function()
    for _,test in ipairs({
        {metadata.new(),metadata.new()},
        {{metadata.fromPairs("foo", "bar"),metadata.fromPairs("foo", "baz")},metadata.fromPairs("foo", "bar", "foo", "baz")},
        {{metadata.fromPairs("foo", "bar"), metadata.fromPairs("foo", "baz")}, metadata.fromPairs("foo", "bar", "foo", "baz")},
        {{metadata.fromPairs("foo", "bar"), metadata.fromPairs("foo", "baz"), metadata.fromPairs("zip", "zap")}, metadata.fromPairs("foo", "bar", "foo", "baz", "zip", "zap")},
    }) do
        testing.equal(metadata.join(table.unpack(test[1])),test[2])
    end
end